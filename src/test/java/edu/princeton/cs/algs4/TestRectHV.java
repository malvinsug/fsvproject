package edu.princeton.cs.algs4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import edu.princeton.cs.algs4.fundamental.RectHV;
import edu.princeton.cs.algs4.fundamental.basic.Point2D;

public class TestRectHV {
	
	private static final double EPSILON = 0; 
	
	private double firstXMin = -100.75;
	private double firstXMax = 99.25;
	private double firstYMin = -99.35;
	private double firstYMax = 0.65;
	private RectHV firstRect;
	
	@BeforeEach
	void setup() {
		//Initialisierung
		firstXMin = -100.75;
		firstXMax = 99.25;
		firstYMin = -99.35;
		firstYMax = 0.65;
		firstRect = new RectHV(firstXMin,firstYMin,firstXMax,firstYMax);
	}
	
	//expected = distanceSquaredTo(point02,edgePoint);
	//distanceSquared = firstRect.distanceSquaredTo();
	//assertEquals(expected,distanceSquared,EPSILON);
	//expected = distanceSquaredTo(point02,edgePoint);
	//distanceSquared = firstRect.distanceSquaredTo();
	//assertEquals(expected,distanceSquared,EPSILON);
	//expected = distanceSquaredTo(point02,edgePoint);
	//distanceSquared = firstRect.distanceSquaredTo();
	//assertEquals(expected,distanceSquared,EPSILON);
	//expected = distanceSquaredTo(point02,edgePoint);
	//distanceSquared = firstRect.distanceSquaredTo();
	//assertEquals(expected,distanceSquared,EPSILON);
	//TODO: Die Initialisierung
	//TODO: xmax()
	//TODO: xmin()
	//TODO: ymax()
	//TODO: ymin()
	//TODO: width()
	//TODO: height()
	@Test
	@DisplayName("correctness_of_object's_initialisation")
	void test01() {
		assertEquals(firstXMin,firstRect.xmin(),EPSILON);
		assertEquals(firstXMax,firstRect.xmax(),EPSILON);
		assertEquals(firstYMin,firstRect.ymin(),EPSILON);
		assertEquals(firstYMax,firstRect.ymax(),EPSILON);
		
		double expectedWidth = firstXMax-firstXMin;
		assertEquals(expectedWidth,firstRect.width(),EPSILON);
		
		double expectedHeight= firstYMax-firstYMin;
		assertEquals(expectedHeight,firstRect.height(),EPSILON);
		
		assertThrows(IllegalArgumentException.class,()-> new RectHV(5,-5,-5,5));
		assertThrows(IllegalArgumentException.class,()-> new RectHV(-5,5,5,-5));
		
		double nan = 0.0/0.0;
		assertThrows(IllegalArgumentException.class,()-> new RectHV(3,3,nan,5));
		assertThrows(IllegalArgumentException.class,()-> new RectHV(nan,3,nan,5));
		
		assertThrows(IllegalArgumentException.class,()-> new RectHV(3,3,5,nan));
		assertThrows(IllegalArgumentException.class,()-> new RectHV(3,nan,5,nan));
	}
	
	//TODO: contains(Point2D)
	//TODO: distanceSquaredTo(Point2D)
	//TODO: distanceTo(Point2D)
	//TODO: intersects(RectHV)
	//TODO: draw()
	@Test
	@DisplayName("interaction_with_other_RectHV_and_Point2D")
	void test02() {
		Point2D edgePoint = new Point2D(firstXMin,firstYMin);
		assertTrue(firstRect.contains(edgePoint));
		
		Point2D insidePoint = new Point2D(firstXMin+50,firstYMin+50);
		assertTrue(firstRect.contains(insidePoint));
		
		Point2D outsidePointRight = new Point2D(200,200);
		assertFalse(firstRect.contains(outsidePointRight));
		
		Point2D outsidePointLeft = new Point2D(-200,-200);
		assertFalse(firstRect.contains(outsidePointLeft));
		
		Point2D point01 = new Point2D(firstXMin-1,firstYMin);
		double expected = distanceSquaredTo(point01,edgePoint);
		double distanceSquared = firstRect.distanceSquaredTo(point01);
		assertEquals(expected,distanceSquared,EPSILON);
		
		double distance = Math.sqrt(distanceSquared);
		assertEquals(distance,firstRect.distanceTo(point01),EPSILON);
		
		Point2D point02 = new Point2D(firstXMax + 1,firstYMax);
		edgePoint = new Point2D(firstXMax,firstYMax);
		expected = distanceSquaredTo(point02,edgePoint);
		distanceSquared = firstRect.distanceSquaredTo(point02);
		assertEquals(expected,distanceSquared,EPSILON);
		
		RectHV outsideRect = new RectHV(2,2,4,4);
		assertFalse(firstRect.intersects(outsideRect));
		
		RectHV insideRect = new RectHV(-4,-4,-2,-2);
		assertTrue(firstRect.intersects(insideRect));
		
		
	}
	
	
	//TODO: equals(Object)
	//TODO: hashCode()?
	//TODO: toString()
	@Test
	@DisplayName("overriden_functions")
	void test03() {
		String expectedString = "["+firstXMin+", "+firstXMax
				+"] x [" + firstYMin + ", " + firstYMax + "]";
		assertTrue(firstRect.toString().equals(expectedString));
		
		RectHV other = firstRect;
		assertTrue(firstRect.equals(other));
		
		RectHV nullRect = null;
		assertFalse(firstRect.equals(nullRect));
		
		Object notRectHV = new Object();
		assertFalse(firstRect.equals(notRectHV));
		
		RectHV otherRect = new RectHV(-200,firstYMin,firstXMax,firstYMax);
		assertFalse(firstRect.equals(otherRect));
		
		otherRect = new RectHV(firstXMin,-200,firstXMax,firstYMax);
		assertFalse(firstRect.equals(otherRect));
		
		otherRect = new RectHV(firstXMin,firstYMin,1,firstYMax);
		assertFalse(firstRect.equals(otherRect));
		
		otherRect = new RectHV(firstXMin,firstYMin,firstXMax,1);
		assertFalse(firstRect.equals(otherRect));
		
		otherRect = new RectHV(firstXMin,firstYMin,firstXMax,firstYMax);
		assertTrue(firstRect.equals(otherRect));
		
		int hash1 = ((Double)firstXMin).hashCode();
		int hash2 = ((Double)firstYMin).hashCode();
		int hash3 = ((Double)firstXMax).hashCode();
		int hash4 = ((Double)firstYMax).hashCode();
		int hash = 31*(31*(31*hash1 + hash2) + hash3) + hash4;
		assertEquals(hash,firstRect.hashCode(),EPSILON);
		
	}
	
	private double distanceSquaredTo(Point2D p1, Point2D p2) {
		double x = p1.x()-p2.x();
		double y = p2.y()-p2.y();
		return  x*x + y*y;
	}

}
