package edu.princeton.cs.algs4;

import java.util.ArrayList;
import java.util.Random;


import edu.princeton.cs.algs4.beyond.PatriciaSET;

import net.jqwik.api.ForAll;
import net.jqwik.api.Property;

import net.jqwik.api.constraints.IntRange;


//import static org.junit.jupiter.api.Assertions.*;

public class PBTPatriciaSET {
	
    @Property
    boolean checkAddFunction(@ForAll @IntRange(min = 1, max = 1000) int dataSize) {
    	ArrayList<String> data = StringGenerator.createRandomData(dataSize);
    	PatriciaSET set = new PatriciaSET();
    	boolean containsData = true;
    	for (String element : data) {
    		set.add(element);
    		containsData = containsData && set.contains(element);
    	}
    	return containsData;
    }
    
    @Property
    boolean checkToStringFunction(@ForAll @IntRange(min = 1, max = 1000) int dataSize) {
    	ArrayList<String> data = StringGenerator.createRandomData(dataSize);
    	PatriciaSET set = new PatriciaSET();
    	for (String element : data) {
    		set.add(element);
    	}
    	
    	String stringSet = set.toString();
    	boolean containsString = true;
    	for (String element : data) {
    		containsString = containsString && stringSet.contains(element);
    	}
    	return containsString;
    }
    
    
    @Property
    boolean checkDeleteFunction(@ForAll @IntRange(min = 1,max = 1000) int dataSize) {
    	ArrayList<String> data = StringGenerator.createRandomData(dataSize);
    	PatriciaSET set = new PatriciaSET();
    	for (String element : data) {
    		set.add(element);
    	}
    	
    	int deletedElementSize = 1;
    	if (dataSize > 90) {
    		deletedElementSize = 50;
    	} else if (dataSize > 70) {
    		deletedElementSize = 30;
    	} else if (dataSize > 50) {
    		deletedElementSize = 20;
    	} else if (dataSize > 30) {
    		deletedElementSize = 13;
    	} 
    	
    	ArrayList<String> deletedItems = new ArrayList<String>();
    	
    	for (int i=0; i<deletedElementSize; i++) {
    		deletedItems.add(data.get(i));
    	}
    	
    	for (String item : deletedItems) {
    		set.delete(item);
    		data.remove(item);
    	}
    	
    	return set.size() == data.size();
    }
    
    @Property
    boolean checkIsEmptyFunction(@ForAll @IntRange(min = 1,max = 1000) int dataSize) {
    	ArrayList<String> data = StringGenerator.createRandomData(dataSize);
    	PatriciaSET set = new PatriciaSET();
    	for (String element : data) {
    		set.add(element);
    	}
    	
        ArrayList<String> deletedItems = new ArrayList<String>();
    	
    	for (int i=0; i<data.size(); i++) {
    		deletedItems.add(data.get(i));
    	}
    	
    	for (String item : deletedItems) {
    		data.remove(item);
    		set.delete(item);
    	}
    	
    	return set.isEmpty() == data.isEmpty();
    }
    
    static class StringGenerator {
    	private static final int SMALL_LETTER_LOWER_LIMIT = 97; // 'a' ASCII code
	    private static final int SMALL_LETTER_UPPER_LIMIT = 122; // 'z'ASCII code
	    
	    private static final int CAPS_LETTER_LOWER_LIMIT = 65; // 'A' ASCII code
	    private static final int CAPS_LETTER_UPPER_LIMIT = 90; // 'Z' ASCII code
	    
	    private static final int DIGIT_LOWER_LIMIT = 48; //'0' ASCII code
	    private static final int DIGIT_UPPER_LIMIT = 57; // '9' ASCII code
	    
	    static ArrayList<String> createRandomData(int dataLength) {
	    	ArrayList<String> data = new ArrayList<String>();
	    	for (int i = 0; i<dataLength; i++) {
	    		String newString = createAString(data);
	    		while (data.contains(newString)) {
	    			newString = createAString(data);
	    		} 
	    		data.add(newString);
	    	}
	    	return data;
	    }
    	
    	private static String createAString(ArrayList<String> currentData) {
    		Random random = new Random();
    		int targetLength = random.nextInt(10);
    		while(targetLength == 0) {
	    		targetLength = 1;
	    	}
    	    
    	    StringBuilder buffer;
    	    if (Math.random() > 0.5 || currentData.size() == 0) {
    	    	buffer = createNewString(targetLength);
    	    } else {
    	    	int randomIndex = random.nextInt(currentData.size());
    	    	String selectedWord = currentData.get(randomIndex);
    	    	
    	    	buffer = createSimilarString(selectedWord, targetLength);
    	    }
    	    String generatedString = buffer.toString();
    	    return generatedString;
    	}

		private static StringBuilder createSimilarString(String selectedWord, int targetLength) {
			Random random = new Random();
			int randomIndex = 0;
			if (selectedWord.length() > 1) {
				randomIndex = random.nextInt(selectedWord.length());
				while (randomIndex ==0) {
					randomIndex = random.nextInt(selectedWord.length());
				}
			}
			
			String substring = selectedWord.substring(0, randomIndex);
			
			StringBuilder buffer = new StringBuilder(targetLength);
			buffer.append(substring);
			
			if (targetLength>substring.length()) {
				int remainLength = targetLength - substring.length();
				String remainString = createNewString(remainLength).toString();
				buffer.append(remainString);
			}
			
			return buffer;
		}

		private static StringBuilder createNewString(int targetLength) {
			Random random = new Random();
    	    StringBuilder buffer = new StringBuilder(targetLength);
    	    int selectedLowerLimit = 0;
    	    int selectedUpperLimit = 0;
    	    
    	    for (int i = 0; i < targetLength; i++) {
    	    	double randomFactor = Math.random();
    	    	if (randomFactor < 0.33) {
    	    		selectedLowerLimit = SMALL_LETTER_LOWER_LIMIT;
    	    		selectedUpperLimit = SMALL_LETTER_UPPER_LIMIT;
    	    	} else if (randomFactor <0.67) {
    	    		selectedLowerLimit = CAPS_LETTER_LOWER_LIMIT;
    	    		selectedUpperLimit = CAPS_LETTER_UPPER_LIMIT;
    	    	} else {
    	    		selectedLowerLimit = DIGIT_LOWER_LIMIT;
    	    		selectedUpperLimit = DIGIT_UPPER_LIMIT;
    	    	}
    	        int randomLimitedInt = selectedLowerLimit + (int) 
    	          (random.nextFloat() * (selectedUpperLimit - selectedLowerLimit + 1));
    	        buffer.append((char) randomLimitedInt);
    	    }
			return buffer;
		}
    }
}
