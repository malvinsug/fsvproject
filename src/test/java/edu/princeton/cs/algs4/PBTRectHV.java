package edu.princeton.cs.algs4;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import edu.princeton.cs.algs4.fundamental.RectHV;
import edu.princeton.cs.algs4.fundamental.basic.Point2D;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.DoubleRange;
import net.jqwik.api.constraints.IntRange;

public class PBTRectHV {
	
	@Property
	boolean checkInitialization (@ForAll @DoubleRange(min = -10000, max = -0.001) double minX, 
			                     @ForAll @DoubleRange(min = -10000, max = -0.001) double minY,                     
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxX,
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxY) {
		
		RectHV rect = new RectHV(minX, minY, maxX, maxY);
		boolean isMinXCorrect = minX == rect.xmin();
		boolean isMinYCorrect = minY == rect.ymin();
		boolean isMaxXCorrect = maxX == rect.xmax();
		boolean isMaxYCorrect = maxY == rect.ymax();
		
		boolean isInitializationCorrect = isMinXCorrect && isMinYCorrect 
				                          && isMaxXCorrect && isMaxYCorrect;
		
		return isInitializationCorrect;
	}
	
	@Property
	boolean checkWidthAndHeight (@ForAll @DoubleRange(min = -10000, max = -0.001) double minX, 
			                     @ForAll @DoubleRange(min = -10000, max = -0.001) double minY,                     
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxX,
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxY) {

		RectHV rect = new RectHV(minX, minY, maxX, maxY);
		double width = Math.abs(minX-maxX);
		double height = Math.abs(minY-maxY);
		
		boolean isWidthCorrect = width == rect.width();
		boolean isHeightCorrect = height == rect.height();
		boolean isWidthAndHeightCorrect = isWidthCorrect && isHeightCorrect;
		
		return isWidthAndHeightCorrect;
	}
	
	@Property
	boolean checkContainsFunction (@ForAll @DoubleRange(min = -10000, max = -0.001) double minX, 
			                     @ForAll @DoubleRange(min = -10000, max = -0.001) double minY,                     
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxX,
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxY, 
			                     @ForAll double randomX,
			                     @ForAll double randomY) {
		
		RectHV rect = new RectHV(minX, minY, maxX, maxY);
        Point2D point = new Point2D(randomX,randomY);
		
        boolean isXInsideBound = isInsideBound(randomX,minX,maxX);
        boolean isYInsideBound = isInsideBound(randomY,minY,maxY);
        boolean isInsideRect = isXInsideBound && isYInsideBound;
        
		return isInsideRect == rect.contains(point);
	}
	
	@Property
	boolean checkDistanceFunction (@ForAll @DoubleRange(min = -10000, max = -0.001) double minX, 
			                     @ForAll @DoubleRange(min = -10000, max = -0.001) double minY,                     
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxX,
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxY, 
			                     @ForAll @DoubleRange(min = -10000, max = 10000)double randomX,
			                     @ForAll @DoubleRange(min = -10000, max = 10000)double randomY) {
		
		RectHV rect = new RectHV(minX, minY, maxX, maxY);
        Point2D point = new Point2D(randomX,randomY);
		
        boolean isOnLeftSide = randomX < minX;
        boolean isOnRightSide = randomX > maxX;
        boolean isOnTopSide = randomY > maxY;
        boolean isOnDownSide = randomY < minY;
        
        boolean isOnLeftTopSide = isOnLeftSide && isOnTopSide;
        boolean isOnLeftDownSide = isOnLeftSide && isOnDownSide;
        boolean isOnRightTopSide = isOnRightSide && isOnTopSide;
        boolean isOnRightDownSide = isOnRightSide && isOnDownSide;
        
        boolean isOnlyOnLeftSide = isOnLeftSide && (!isOnTopSide && !isOnDownSide);
        boolean isOnlyOnRightSide = isOnRightSide && (!isOnTopSide && !isOnDownSide); 
        boolean isOnlyOnTopSide = isOnTopSide && (!isOnLeftSide && !isOnRightSide);
        boolean isOnlyOnDownSide = isOnDownSide && (!isOnLeftSide && !isOnRightSide);
        
        double distance = 0;
        if (isOnlyOnLeftSide) {
        	distance = Math.abs(randomX-minX);
        } else if (isOnlyOnRightSide) {
        	distance = Math.abs(randomX-maxX);
        } else if (isOnlyOnTopSide) {
        	distance = Math.abs(randomY-maxY);
        } else if (isOnlyOnDownSide) {
        	distance = Math.abs(randomY-minY);
        } else if (isOnLeftTopSide) {
        	double deltaX = Math.abs(randomX-minX);
        	double deltaY = Math.abs(randomY-maxY);
        	distance = Math.abs(Math.sqrt(deltaX*deltaX + deltaY*deltaY));
        } else if (isOnRightTopSide) {
        	double deltaX = Math.abs(randomX-maxX);
        	double deltaY = Math.abs(randomY-maxY);
        	distance = Math.abs(Math.sqrt(deltaX*deltaX + deltaY*deltaY));
        } else if (isOnRightDownSide) {
        	double deltaX = Math.abs(randomX-maxX);
        	double deltaY = Math.abs(randomY-minY);
        	distance = Math.sqrt(deltaX*deltaX + deltaY*deltaY);
        } else if (isOnLeftDownSide) {
        	double deltaX = Math.abs(randomX-minX);
        	double deltaY = Math.abs(randomY-minY);
        	distance = Math.sqrt(deltaX*deltaX + deltaY*deltaY);
        } else {
        	distance = 0;
        }
        
		return distance == rect.distanceTo(point);
	}
	
	@Property (tries = 100000)
	boolean checkIntersects (@ForAll @DoubleRange(min = -10000, max = -0.001) double minX1, 
			                     @ForAll @DoubleRange(min = -10000, max = -0.001) double minY1,                     
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxX1,
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxY1,
			                     @ForAll @DoubleRange(min = -10000, max = -0.001) double minX2, 
			                     @ForAll @DoubleRange(min = -10000, max = -0.001) double minY2,                     
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxX2,
			                     @ForAll @DoubleRange(min = 0, max = 10000) double maxY2) {
		
		RectHV rect1 = new RectHV(minX1, minY1, maxX1, maxY1);
		RectHV rect2 = new RectHV(minX2, minY2, maxX2, maxY2);
		
		Point2D point1 = new Point2D(minX1,minY1);
		Point2D point2 = new Point2D(minX1,maxY1);
		Point2D point3 = new Point2D(maxX1,maxY1);
		Point2D point4 = new Point2D(maxX1,minY1);
		
		Point2D point5 = new Point2D(minX2,minY2);
		Point2D point6 = new Point2D(minX2,maxY2);
		Point2D point7 = new Point2D(maxX2,maxY2);
		Point2D point8 = new Point2D(maxX2,minY2);
		
		ArrayList<Point2D> pointsRect1 = new ArrayList<Point2D>();
		ArrayList<Point2D> pointsRect2 = new ArrayList<Point2D>();
		pointsRect1.add(point1);
		pointsRect1.add(point2);
		pointsRect1.add(point3);
		pointsRect1.add(point4);
		
		pointsRect2.add(point5);
		pointsRect2.add(point6);
		pointsRect2.add(point7);
		pointsRect2.add(point8);
		
		boolean isIntersecting = true;
		for (Point2D point : pointsRect2) {
			isIntersecting = isIntersecting || rect1.contains(point);
		}
		for (Point2D point : pointsRect1) {
			isIntersecting = isIntersecting || rect2.contains(point);
		}
		
		return isIntersecting == rect1.intersects(rect2) == rect2.intersects(rect1);
	}
	
	@Property
	boolean checkEquals(@ForAll @IntRange(min = 0, max = 1000) int randomNumber,
			            @ForAll @DoubleRange(min = -10000, max = -0.001) double minX, 
                        @ForAll @DoubleRange(min = -10000, max = -0.001) double minY,                     
                        @ForAll @DoubleRange(min = 0, max = 10000) double maxX,
                        @ForAll @DoubleRange(min = 0, max = 10000) double maxY) {
		
		double xmin = -100;
		double ymin = -100;
		double xmax = 100;
		double ymax = 100;
		
		RectHV rect = new RectHV(xmin, ymin, xmax, ymax);
		
		boolean isEqual;
		if (randomNumber > 800) {
			RectHV other = rect;
			isEqual = (other == rect) == rect.equals(rect);
		} else if (randomNumber > 600) {
			RectHV other = null;
			isEqual = (other == null) == !rect.equals(other);
		} else if (randomNumber > 500) {
			Point2D randomObject = new Point2D(maxX,maxY);
			isEqual = (randomObject.getClass().equals(RectHV.class)) == rect.equals(randomObject);
		} else if (randomNumber > 400) {
			RectHV other = new RectHV(minX,minY,maxX,maxY);
			isEqual = (xmin == other.xmin() && xmax == other.xmax()
					  && ymin == other.ymin() && ymax == other.ymax()) == rect.equals(other);
		} else if (randomNumber > 300){
			RectHV other = new RectHV(xmin,minY,maxX,maxY);
			isEqual = (xmin == other.xmin() && xmax == other.xmax()
					  && ymin == other.ymin() && ymax == other.ymax()) == rect.equals(other);
		} else if (randomNumber >200){
			RectHV other = new RectHV(xmin,ymin,maxX,maxY);
			isEqual = (xmin == other.xmin() && xmax == other.xmax()
					  && ymin == other.ymin() && ymax == other.ymax()) == rect.equals(other);
		} else if (randomNumber >100){
			RectHV other = new RectHV(xmin,ymin,xmax,maxY);
			isEqual = (xmin == other.xmin() && xmax == other.xmax()
					  && ymin == other.ymin() && ymax == other.ymax()) == rect.equals(other);
		} else {
			RectHV other = new RectHV(xmin,ymin,xmax,ymax);
			isEqual = (xmin == other.xmin() && xmax == other.xmax()
					  && ymin == other.ymin() && ymax == other.ymax()) == rect.equals(other);
		}
		
		return isEqual;
	}
	
	@Property
	boolean checkString(@ForAll @DoubleRange(min = -10000, max = -0.001) double minX, 
                        @ForAll @DoubleRange(min = -10000, max = -0.001) double minY,                     
                        @ForAll @DoubleRange(min = 0, max = 10000) double maxX,
                        @ForAll @DoubleRange(min = 0, max = 10000) double maxY) {
	    
		RectHV rect = new RectHV(minX, minY, maxX, maxY);
		String rectString = rect.toString();
		String manualString = "[" + minX + ", " + maxX + "] x [" + minY + ", " + maxY + "]";
		return manualString.equals(rectString);
	}
	
	@Property
	boolean checkHashCode(@ForAll @DoubleRange(min = -10000, max = -0.001) double minX, 
                        @ForAll @DoubleRange(min = -10000, max = -0.001) double minY,                     
                        @ForAll @DoubleRange(min = 0, max = 10000) double maxX,
                        @ForAll @DoubleRange(min = 0, max = 10000) double maxY) {
	    
		RectHV rect = new RectHV(minX, minY, maxX, maxY);
		int rectHashCode = rect.hashCode();
		
		int hash1 = ((Double) minX).hashCode();
        int hash2 = ((Double) minY).hashCode();
        int hash3 = ((Double) maxX).hashCode();
        int hash4 = ((Double) maxY).hashCode();
        
		int manualHashCode = 31*(31*(31*hash1 + hash2) + hash3) + hash4;
		
		return manualHashCode == rectHashCode;
	}
	
	boolean isInsideBound(double value, double lowerBound,double upperBound) {
		return value >= lowerBound && value <= upperBound;
	}
	
	
	
}
