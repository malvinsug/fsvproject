package edu.princeton.cs.algs4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.princeton.cs.algs4.fundamental.basic.Stack;
import edu.princeton.cs.algs4.graphs.EulerianCycle;
import edu.princeton.cs.algs4.graphs.basic.Graph;
import edu.princeton.cs.algs4.graphs.basic.BreadthFirstPaths;
import edu.princeton.cs.algs4.graphs.basic.GraphGenerator;
import edu.princeton.cs.algs4.util.StdOut;
import edu.princeton.cs.algs4.util.StdRandom;

class TestEulerianCycle {
	
	private Graph g01;
	private Graph g02;
	private Graph emptyGraph;
	private EulerianCycle eulerCycle;
	
	@BeforeEach
	void setup() {
        //Diese Graph hat mindestens einen Eulerkreis
        // This graph has at least one euler circle.
        Graph g00 = new Graph(5);
		g01 = new Graph(6);
		g01.addEdge(0, 1);
		g01.addEdge(0, 2);
		g01.addEdge(0, 3);
		g01.addEdge(0, 5);
		g01.addEdge(1, 2);
		g01.addEdge(1, 3);
		g01.addEdge(1, 4);
		g01.addEdge(2, 4);
		g01.addEdge(2, 5);
		g01.addEdge(3, 4);
		g01.addEdge(3, 5);
		g01.addEdge(4, 5);
		
		// Diese Graph hat keine Eulerkreis
		g02 = new Graph(6);
		g02.addEdge(0,1);
		g02.addEdge(0,2);
		g02.addEdge(1,2);
		g02.addEdge(1,4);
		g02.addEdge(1,5);
		g02.addEdge(2,3);
		g02.addEdge(2,4);
		g02.addEdge(3,4);
		g02.addEdge(4,4);
		g02.addEdge(4,5);
		g02.addEdge(5,5);
		
		emptyGraph = new Graph(4);
	}
	
	@Test
	void test() {
       eulerCycle = new EulerianCycle(emptyGraph);
       assertFalse(eulerCycle.hasEulerianCycle());
       
       eulerCycle = new EulerianCycle(g01);
       assertTrue(eulerCycle.hasEulerianCycle());
       
       eulerCycle = new EulerianCycle(g02);
       assertTrue(eulerCycle.hasEulerianCycle());
       System.out.println(eulerCycle.cycle());
       
       g02.addEdge(2, 4);
       eulerCycle = new EulerianCycle(g02);
       assertFalse(eulerCycle.hasEulerianCycle());
	}
	
	// Ich habe die folgende Methode hier eingefugt, weil ich finde es sinvoller, 
	// wenn man die Testen in einem Ordner einpackt. Außerdem man kann auch ein paar 
	// Methoden nutzt, um einen PBTest durchzuführen.
	/**************************************************************************
    *
    *  The code below is solely for testing correctness of the data type. 
    *
    **************************************************************************/
	
	// Determines whether a graph has an Eulerian cycle using necessary
    // and sufficient conditions (without computing the cycle itself):
    //    - at least one edge
    //    - degree(v) is even for every vertex v
    //    - the graph is connected (ignoring isolated vertices)
	static boolean satisfiesNecessaryAndSufficientConditions(Graph G) {

        // Condition 0: at least 1 edge
        if (G.E() == 0) return false;

        // Condition 1: degree(v) is even for every vertex
        for (int v = 0; v < G.V(); v++)
            if (G.degree(v) % 2 != 0)
                return false;

        // Condition 2: graph is connected, ignoring isolated vertices
        int s = EulerianCycle.findNonIsolatedVertex(G);
        BreadthFirstPaths bfs = new BreadthFirstPaths(G, s);
        for (int v = 0; v < G.V(); v++)
            if (G.degree(v) > 0 && !bfs.hasPathTo(v))
                return false;

        return true;
    }

   // check that solution is correct
   static boolean certifySolution(Graph G) {
	   
	   EulerianCycle eulerCycle = new EulerianCycle(G);

       
       // internal consistency check
       if (eulerCycle.hasEulerianCycle() == (eulerCycle.cycle() == null)) return false;

       // hashEulerianCycle() returns correct value
       if (eulerCycle.hasEulerianCycle() != satisfiesNecessaryAndSufficientConditions(G)) return false;

       // nothing else to check if no Eulerian cycle
       if (eulerCycle.cycle() == null) return true;

       // check that cycle() uses correct number of edges
       Stack<Integer> cycle = (Stack<Integer>) eulerCycle.cycle();
       if (cycle.size() != G.E() + 1) return false;

       // check that cycle() is a cycle of G
       // TODO

       // check that first and last vertices in cycle() are the same
       int first = -1, last = -1;
       for (int v : eulerCycle.cycle()) {
           if (first == -1) first = v;
           last = v;
       }
       if (first != last) return false;

       return true;
   }

   private static void unitTest(Graph G, String description) {
       StdOut.println(description);
       StdOut.println("-------------------------------------");
       StdOut.print(G);

       EulerianCycle euler = new EulerianCycle(G);

       StdOut.print("Eulerian cycle: ");
       if (euler.hasEulerianCycle()) {
           for (int v : euler.cycle()) {
               StdOut.print(v + " ");
           }
           StdOut.println();
       }
       else {
           StdOut.println("none");
       }
       StdOut.println();
   }

   /**
    * Unit tests the {@code EulerianCycle} data type.
    *
    * @param args the command-line arguments
    */
   public static void main(String[] args) {
       int V = Integer.parseInt(args[0]);
       int E = Integer.parseInt(args[1]);

       // Eulerian cycle
       Graph G1 = GraphGenerator.eulerianCycle(V, E);
       unitTest(G1, "Eulerian cycle");

       // Eulerian path
       Graph G2 = GraphGenerator.eulerianPath(V, E);
       unitTest(G2, "Eulerian path");

       // empty graph
       Graph G3 = new Graph(V);
       unitTest(G3, "empty graph");

       // self loop
       Graph G4 = new Graph(V);
       int v4 = StdRandom.uniform(V);
       G4.addEdge(v4, v4);
       unitTest(G4, "single self loop");

       // union of two disjoint cycles
       Graph H1 = GraphGenerator.eulerianCycle(V/2, E/2);
       Graph H2 = GraphGenerator.eulerianCycle(V - V/2, E - E/2);
       int[] perm = new int[V];
       for (int i = 0; i < V; i++)
           perm[i] = i;
       StdRandom.shuffle(perm);
       Graph G5 = new Graph(V);
       for (int v = 0; v < H1.V(); v++)
           for (int w : H1.adj(v))
               G5.addEdge(perm[v], perm[w]);
       for (int v = 0; v < H2.V(); v++)
           for (int w : H2.adj(v))
               G5.addEdge(perm[V/2 + v], perm[V/2 + w]);
       unitTest(G5, "Union of two disjoint cycles");

       // random digraph
       Graph G6 = GraphGenerator.simple(V, E);
       unitTest(G6, "simple graph");
   }
}