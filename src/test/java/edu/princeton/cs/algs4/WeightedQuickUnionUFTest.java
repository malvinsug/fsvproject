package edu.princeton.cs.algs4;

import java.util.Random;
import java.util.stream.IntStream;

import org.assertj.core.api.Assertions;

import edu.princeton.cs.algs4.fundamental.basic.WeightedQuickUnionUF;
import net.jqwik.api.Arbitraries;
import net.jqwik.api.Arbitrary;
import net.jqwik.api.Assume;
import net.jqwik.api.Disabled;
import net.jqwik.api.ForAll;
import net.jqwik.api.Group;
import net.jqwik.api.Property;
import net.jqwik.api.Provide;
import net.jqwik.api.RandomGenerator;
import net.jqwik.api.Shrinkable;
import net.jqwik.api.ShrinkingMode;
import net.jqwik.api.constraints.Negative;
import net.jqwik.api.constraints.Positive;
import net.jqwik.api.stateful.Action;
import net.jqwik.api.stateful.ActionSequence;
import net.jqwik.api.stateful.ActionSequenceArbitrary;
import net.jqwik.api.statistics.Statistics;

public class WeightedQuickUnionUFTest {
	
	@Property
	void outer(@ForAll int n) {
	}

    @Group
    @Disabled
    class GroupInitialisation {
    	
	
    	@Property(tries = 1000, seed = "2")
		void ufCountIsEqualToInstantiationValue(@ForAll @Positive short n) { 
			Assertions.assertThat(new WeightedQuickUnionUF(n).count()).isEqualTo(n);
		}

		@Property
		void ufCountIsAlwaysPositive(@ForAll  @Positive short n) { 
			Assertions.assertThat(new WeightedQuickUnionUF(n).count()).isGreaterThanOrEqualTo(0);
		}

		@Property
		void throwsIllegalArgumentExceptionOnNegativeSize(@ForAll @Negative int n) { 
			Assertions.assertThatThrownBy(() -> new WeightedQuickUnionUF(n)).isInstanceOf(NegativeArraySizeException.class);
		}
		

    }
	
    @Group
    @Disabled
    class GroupFind {
    	
    
    	@Property
		void throwsIllegalArgumentExceptionOnNegativeFind(@ForAll @Positive short n, @ForAll @Negative int p) { 
			Assertions.assertThatThrownBy(() ->  new WeightedQuickUnionUF(n).find(p)).isInstanceOf(IllegalArgumentException.class);
		}
	
		@Property
		void throwsIllegalArgumentExceptionOnOutOfBoundFind(@ForAll @Positive short n, @ForAll @Positive int p) { 
			int tmpMax = Integer.max(n, p);
			int tmpMin = Integer.min(n, p);
			Assertions.assertThatThrownBy(() -> new WeightedQuickUnionUF(tmpMin).find(tmpMax)).isInstanceOf(IllegalArgumentException.class);
		}
	
		@Property
    	void findReturnsCanonicalElementOfContainingSet(@ForAll @Positive short n, @ForAll @Positive short p){
			Assume.that(n > p);
			Assertions.assertThat(new WeightedQuickUnionUF(n).find(p)).isEqualTo(p);
		}
    }
    
    @Group
    class GroupUnion {
    	
    
    	@Property
		void throwsIllegalArgumentExceptionOnNegativeUnion(@ForAll("provideUnionUF") WeightedQuickUnionUF unionUF, @ForAll int p, @ForAll int q) { 
			int n = unionUF.count();
    		if(p < q)
    			Assume.that(p < 0);
    		else
    			Assume.that(q < 0);
			Assertions.assertThatThrownBy(() ->  unionUF.union(p, q)).isInstanceOf(IllegalArgumentException.class);
		}
	
		@Property
		void throwsIllegalArgumentExceptionOnOutOfBoundUnion(@ForAll("provideUnionUF") WeightedQuickUnionUF unionUF, @ForAll int p, @ForAll int q) { 
			int n = unionUF.count();
			if(p < q)
    			Assume.that(q > n);
    		else
    			Assume.that(p > n);
			Assertions.assertThatThrownBy(() -> unionUF.union(p, q)).isInstanceOf(IllegalArgumentException.class);
		}
	
		@Property
    	void unionOfUnequalReducesCount(@ForAll("provideUnionUF") WeightedQuickUnionUF unionUF, @ForAll @Positive short p, @ForAll @Positive short q){
			int n = unionUF.count();
			Assume.that(n > p && n > q && p != q);
			int oldCount = unionUF.count();
			unionUF.union(p, q);
			Assertions.assertThat(unionUF.count()).isEqualTo(oldCount-1);
		}
		
		@Property
    	void unionOfEqualNotReducesCount(@ForAll("provideUnionUF") WeightedQuickUnionUF unionUF, @ForAll @Positive short p){
			int n = unionUF.count();
			Assume.that(n > p);
			int q = p;
			int oldCount = unionUF.count();
			unionUF.union(p, q);
			Assertions.assertThat(unionUF.count()).isEqualTo(oldCount);
		}
		
		@Property
    	void mergedSetsAreInTheSameSuperSet(@ForAll("provideUnionUF") WeightedQuickUnionUF unionUF, @ForAll @Positive short p, @ForAll @Positive short q){
			int n = unionUF.count();
			Assume.that(n > p && n > q && p != q);
			unionUF.union(p, q);
			Assertions.assertThat(unionUF.find(p)).isEqualTo(unionUF.find(q));
		}
		
		
		@Property
    	void mergedSetsAreInTheGreaterSuperSet(@ForAll("provideUnionUF") WeightedQuickUnionUF unionUF, @ForAll @Positive short p, @ForAll @Positive short q){
			
			int n = unionUF.count();
			Assume.that(n > p && n > q && p != q);
			int greater = p > q ? p : q;
			unionUF.union(p, q);

			Assertions.assertThat(unionUF.find(p)).isEqualTo(unionUF.find(greater));			
			Assertions.assertThat(unionUF.find(q)).isEqualTo(unionUF.find(greater));
			
		    String evenOrOdd = n % 2 == 0 ? "even" : "odd";
		    String bigOrSmall = Math.abs(n) > 1000 ? "big" : "small";
		    String pOrQgreater = p > q ? "p-Greater" : "q-Greater";
		    Statistics.collect(pOrQgreater);		    

		    Statistics.collect(evenOrOdd);		    
		    Statistics.collect(bigOrSmall);
		}
		
		@Property(shrinking = ShrinkingMode.OFF)
		void checkUnionActionsWithInvariant(@ForAll("provideUnionUF") WeightedQuickUnionUF unionUF) {
			Assume.that(unionUF.count() > 1);
			int n = unionUF.count();
			ActionSequenceArbitrary<WeightedQuickUnionUF> actionsArbitrary = provideUnionActions(n);
			actionsArbitrary.sampleStream().limit(10)
			.forEach( actions -> actions.withInvariant(uF -> checkIfAllContainingSetsAreGreaterOrEqualToElement(uF, n))
					.run(unionUF));
			
		}
		
	
		void checkIfAllContainingSetsAreGreaterOrEqualToElement(WeightedQuickUnionUF unionUF, int initialSize) {
			Assertions.assertThat(unionUF.count()).isLessThanOrEqualTo(initialSize);
		}
		
		
	
		@Provide
		Arbitrary<WeightedQuickUnionUF> provideUnionUF() {
		    return Arbitraries.shorts().filter(i -> i > 0)
		    		.map(i -> new WeightedQuickUnionUF(i));
		}
		
		@Provide
		ActionSequenceArbitrary<WeightedQuickUnionUF> provideUnionActions(int n) {
			return Arbitraries.sequences(Arbitraries.fromGenerator(new UnionActionGenerator(n)));
		}
		
		
		class UnionActionGenerator implements RandomGenerator<UnionAction>{
			
			private int n;
			
			UnionActionGenerator(int n){
				this.n = n;
			}

			@Override
			public Shrinkable<UnionAction> next(Random random) {
				int p = random.nextInt(n);
				int q = random.nextInt(n);

				return Shrinkable.unshrinkable(new UnionAction(p , q));
			}
		
	
		}
		
		
		class UnionAction implements Action<WeightedQuickUnionUF> {
			
			private int p, q;
			
			UnionAction( int p, int q){
				this.p = p;
				this.q = q;
			}

			@Override
			public WeightedQuickUnionUF run(WeightedQuickUnionUF state) {
				state.union(p, q);
				return state;
			}

			
			public String toString() {
				return "(p/q)=("+ p +"/" + q +")";
			}
		}
		
    }

}