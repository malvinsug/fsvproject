package edu.princeton.cs.algs4;

import edu.princeton.cs.algs4.graphs.EulerianCycle;
import edu.princeton.cs.algs4.graphs.basic.Graph;
import edu.princeton.cs.algs4.graphs.basic.GraphGenerator;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.IntRange;
import net.jqwik.api.constraints.Positive;

//import static org.junit.jupiter.api.Assertions.*;

public class PBTEulerianCycle {
	
	@Property
	boolean checkSatisfiabilityNecessaryAndSufficientConditions(@ForAll @IntRange(min = 1, max = 100000) int V, 
			                                                    @ForAll @IntRange(min = 1, max = 100000) int E) {
		
		System.out.println(200); // fix
		System.out.println(300); // new feature
		System.out.println(400); // what is perf?
		System.out.println(500); // breaking change
		System.out.println(600); // try fix and see if it triggers major release
        System.out.println(700); // try breaking change again

	    Graph checkGraph = GraphGenerator.eulerianCycle(V, E);
		EulerianCycle eulerianCycle = new EulerianCycle(checkGraph);
		boolean isSatisfied = TestEulerianCycle.satisfiesNecessaryAndSufficientConditions(checkGraph) 
				              && eulerianCycle.hasEulerianCycle() ;
		return isSatisfied;
	}
	
	@Property
	boolean checkCertifySolution(@ForAll @IntRange(min = 1, max = 100000) int V, 
			                     @ForAll @IntRange(min = 1, max = 100000) int E) {
		Graph checkGraph = GraphGenerator.eulerianCycle(V, E);
		EulerianCycle eulerianCycle = new EulerianCycle(checkGraph);
		boolean isSatisfied = TestEulerianCycle.certifySolution(checkGraph) 
	                          && eulerianCycle.hasEulerianCycle() ;
		return isSatisfied;
	}
}