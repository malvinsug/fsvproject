package edu.princeton.cs.algs4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.princeton.cs.algs4.beyond.PatriciaSET;
import edu.princeton.cs.algs4.util.StdOut;
import edu.princeton.cs.algs4.util.StdRandom;

class TestPatriciaSET {
	
	private PatriciaSET patSET;
	
	@BeforeEach
	void setup() {
		patSET = new PatriciaSET();
	}
	
	@Test
	void test01() {
		assertThrows(IllegalArgumentException.class,()-> patSET.add(null));
		assertThrows(IllegalArgumentException.class,()->patSET.add(""));
		
		boolean isDataCorrect = patSET.toString().equals("");
		assertTrue(isDataCorrect);
		
		patSET.add("POKANE");
		patSET.add("POKANUS");
		patSET.add("POKULUS");
		patSET.add("PUDENS");
		patSET.add("PUDEP");
		patSET.add("PUDICON");
		patSET.add("PUDICUNDUS");
		patSET.add("PUD");
				
		assertTrue(patSET.contains("POKANE"));
		assertTrue(patSET.contains("POKANUS"));
		assertTrue(patSET.contains("POKULUS"));
		assertTrue(patSET.contains("PUDENS"));
		assertTrue(patSET.contains("PUDEP"));
		assertTrue(patSET.contains("PUDICON"));
		assertTrue(patSET.contains("PUDICUNDUS"));
		assertTrue(patSET.contains("PUD"));
				
		assertThrows(IllegalArgumentException.class,()-> patSET.contains(null));
		assertThrows(IllegalArgumentException.class,()->patSET.contains(""));
		
		isDataCorrect = patSET.toString().equals("PUDICUNDUS PUDICON PUDEP PUD PUDENS POKANUS POKULUS POKANE");
		assertTrue(isDataCorrect);
				
		patSET.delete("POKANE");
		patSET.delete("POKANUS");
		patSET.delete("POKULUS");
		patSET.delete("PUDENS");
		patSET.delete("PUDEP");
		patSET.delete("PUDICON");
		patSET.delete("PUDICUNDUS");
		patSET.delete("PUD");
				
		assertFalse(patSET.contains("POKANE"));
		assertFalse(patSET.contains("POKANUS"));
		assertFalse(patSET.contains("POKULUS"));
		assertFalse(patSET.contains("PUDENS"));
		assertFalse(patSET.contains("PUDEP"));
		assertFalse(patSET.contains("PUDICON"));
		assertFalse(patSET.contains("PUDICUNDUS"));
		assertFalse(patSET.contains("PUD"));
		
		assertThrows(IllegalArgumentException.class,()-> patSET.delete(null));
		assertThrows(IllegalArgumentException.class,()->patSET.delete(""));
		
		
	}
    
    void test02() {
        System.out.println(123);
        patSET.add("POKANE");
        
        patSET.add("POKANUS");
        
        patSET.add("POKULUS");
        
        patSET.add("PUDENS");
        
		patSET.add("PUDEP");
		patSET.add("PUDICON");
		patSET.add("PUDICUNDUS");
		patSET.add("PUD");
				
		assertTrue(patSET.contains("POKANE"));
        assertTrue(patSET.contains("POKANUS"));
        
        assertTrue(patSET.contains("POKULUS"));
        
		assertTrue(patSET.contains("PUDENS"));
		assertTrue(patSET.contains("PUDEP"));
        assertTrue(patSET.contains("PUDICON"));
        
		assertTrue(patSET.contains("PUDICUNDUS"));
		assertTrue(patSET.contains("PUD"));
    }

	/**
     * Unit tests the {@code PatriciaSET} data type.
     * This test fixture runs a series of tests on a randomly generated dataset.
     * You may specify up to two integer parameters on the command line. The
     * first parameter indicates the size of the dataset. The second parameter
     * controls the number of passes (a new random dataset becomes generated at
     * the start of each pass).
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {
        PatriciaSET set = new PatriciaSET();
        int limitItem = 1000000;
        int limitPass = 1;
        int countPass = 0;
        boolean ok = true;

        if (args.length > 0) limitItem = Integer.parseInt(args[0]);
        if (args.length > 1) limitPass = Integer.parseInt(args[1]);

        do {
            String[] a = new String[limitItem];

            StdOut.printf("Creating dataset (%d items)...\n", limitItem);
            for (int i = 0; i < limitItem; i++)
                a[i] = Integer.toString(i, 16);
            
            StdOut.printf("Shuffling...\n");
            StdRandom.shuffle(a);

            StdOut.printf("Adding (%d items)...\n", limitItem);
            for (int i = 0; i < limitItem; i++)
                set.add(a[i]);

            int countItems = 0;
            StdOut.printf("Iterating...\n");
            for (String key : set) countItems++;
            StdOut.printf("%d items iterated\n", countItems);
            if (countItems != limitItem)  ok = false;
            if (countItems != set.size()) ok = false;

            StdOut.printf("Shuffling...\n");
            StdRandom.shuffle(a);

            int limitDelete = limitItem / 2;
            StdOut.printf("Deleting (%d items)...\n", limitDelete);
            for (int i = 0; i < limitDelete; i++)
                set.delete(a[i]);

            countItems = 0;
            StdOut.printf("Iterating...\n");
            for (String key : set) countItems++;
            StdOut.printf("%d items iterated\n", countItems);
            if (countItems != limitItem - limitDelete) ok = false;
            if (countItems != set.size())              ok = false;

            int countDelete = 0;
            int countRemain = 0;
            StdOut.printf("Checking...\n");
            for (int i = 0; i < limitItem; i++) {
                if (i < limitDelete) {
                    if (!set.contains(a[i])) countDelete++;
                }
                else {
                    if (set.contains(a[i])) countRemain++;
                }
            }
            StdOut.printf("%d items found and %d (deleted) items missing\n",
                countRemain, countDelete);
            if (countRemain + countDelete != limitItem)  ok = false;
            if (countRemain               != set.size()) ok = false;
            if (set.isEmpty())                           ok = false;

            StdOut.printf("Deleting the rest (%d items)...\n",
                limitItem - countDelete);
            for (int i = countDelete; i < limitItem; i++)
                set.delete(a[i]);
            if (!set.isEmpty()) ok = false;

            countPass++;
            if (ok) StdOut.printf("PASS %d TESTS SUCCEEDED\n", countPass);
            else    StdOut.printf("PASS %d TESTS FAILED\n",    countPass);
        } while (ok && countPass < limitPass);

        if (!ok) throw new java.lang.RuntimeException("TESTS FAILED");
    }
	
}